import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  data: any;
  columns: string[];
  filterColumns = [
    {
      displayName: 'Name',
      actualName: 'name'
    },
    {
      displayName: 'Severity',
      actualName: 'country_check_severity'
    }
  ];
  searchValue = '';
  searchFilter = this.filterColumns[0].displayName;
  sortColumn = {
    name: '',
    direction: 'asc'
  };
  sortableColumns = ['name', 'created', 'modified'];

  constructor( private dataService: DataService) {
  }

  ngOnInit() {
    this.columns = this.dataService.getColumns();
    this.data = this.dataService.getData();
    this.sort('name', 'asc');
  }

  sort(column: string, direction: string) {
    this.sortColumn.name = column;
    if (direction !== null) {
      this.sortColumn.direction = direction;
    } else {
      this.sortColumn.direction = this.sortColumn.direction === 'asc' ? 'desc' : 'asc';
    }
    this.data.results = this.data.results.sort((x, y) => {
      const status = x[column] > y[column];
      if (this.sortColumn.direction === 'asc') {
        return status ? 1 : -1;
      } else {
        return status ? -1 : 1;
      }
    });
  }

  isSortableColumn(col: string) {
    return this.sortableColumns.includes(col);
  }
}
