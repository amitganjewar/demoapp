import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { data } from './app.component.data';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {
  }

  getData(): any {
    return data;
  }

  getColumns(): string[] {
    return ["name", "severity", "created", "modified"];
  }
}
