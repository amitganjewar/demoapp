import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'tableFilterPipe' })
export class TableFilterPipe implements PipeTransform {
  transform(arrayToBeProcess: any[], filterColumn: string, filterValue: string) {

    if (filterValue === undefined || filterValue === '') {
      return  arrayToBeProcess;
    }
    arrayToBeProcess = arrayToBeProcess.filter(item => {
      return  ((item[filterColumn].toLowerCase()).includes(filterValue.toLowerCase()));
    });
    return arrayToBeProcess;
  }
}
